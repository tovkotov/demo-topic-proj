package com.example.demo.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
@Log4j2
public class ApplicationExceptionHandler {

    @ExceptionHandler({NoSuchElementException.class})
    public ResponseEntity<HttpStatus> noSuchElementExceptionHandler(RuntimeException e) {
        log.info(java.time.LocalDateTime.now() + " NoSuchElementException: {}", e.getMessage());
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
