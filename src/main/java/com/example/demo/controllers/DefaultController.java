package com.example.demo.controllers;

import com.example.demo.entity.Topic;
import com.example.demo.entity.TopicItem;
import com.example.demo.request.TopicForm;
import com.example.demo.request.TopicItemForm;
import com.example.demo.response.ResultResponse;
import com.example.demo.response.TopicItemResponse;
import com.example.demo.response.TopicResponse;
import com.example.demo.service.TopicItemService;
import com.example.demo.service.TopicService;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class DefaultController {
    private final TopicService topicService;
    private final TopicItemService topicItemService;

    public DefaultController(TopicService topicService,
                             TopicItemService topicItemService) {
        this.topicService = topicService;
        this.topicItemService = topicItemService;
    }

    @GetMapping("/topic")
    public ResponseEntity<ArrayList<TopicResponse>> topicList(
            @RequestParam(defaultValue = "0", required = false) int offset,
            @RequestParam(defaultValue = "5") int limit) {
        return topicService.getTopics(offset, limit);
    }

    @GetMapping("/topic/{id}")
    public ResponseEntity<Topic> getTopic(@PathVariable int id) {
        return topicService.getTopicById(id);
    }

    @GetMapping("/topicItem")
    public ResponseEntity<ArrayList<TopicItemResponse>> topicItemList(
            @RequestParam(defaultValue = "0", required = false) int offset,
            @RequestParam(defaultValue = "5") int limit) {
        return topicItemService.getTopicItems(offset, limit);
    }

    @GetMapping("/topicItem/{id}")
    public ResponseEntity<TopicItem> getTopicItem(@PathVariable int id) {
        return topicItemService.getTopicItemById(id);
    }

    @PostMapping("/topic")
    public ResponseEntity<ResultResponse> addTopic(TopicForm form) {
        return topicService.addTopic(form);
    }

    @PostMapping("/topicItem")
    public ResponseEntity<ResultResponse> addTopicItem(
            TopicItemForm form) throws NotFoundException {
        return topicItemService.addTopicItem(form);
    }

    @PutMapping("/topic/{id}")
    public ResponseEntity<ResultResponse> topicEdit(@PathVariable int id, TopicForm form) {
        return topicService.editTopic(id, form);
    }

    @PutMapping("/topicItem/{id}")
    public ResponseEntity<ResultResponse> topicItemEdit(@PathVariable int id, TopicItemForm form) throws NotFoundException {
        return topicItemService.editTopicItem(id, form);
    }

    @DeleteMapping("/topic/{id}")
    public ResponseEntity<ResultResponse> topicDelete(@PathVariable int id) {
        return topicService.deleteTopic(id);
    }

    @DeleteMapping("/topicItem/{id}")
    public ResponseEntity<ResultResponse> topicItemDelete(@PathVariable int id) throws NotFoundException {
        return topicItemService.deleteTopicItem(id);
    }
}
