CREATE TABLE topics
(
    id          SERIAL PRIMARY KEY NOT NULL,
    name        text               NOT NULL,
    description text               NOT NULL
);

CREATE TABLE topic_items
(
    id          SERIAL PRIMARY KEY NOT NULL,
    name        text               NOT NULL,
    order_topic int                NOT NULL,
    topic_id    int                NOT NULL,
    FOREIGN KEY (topic_id) REFERENCES topics (id) ON DELETE CASCADE
);